<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_E2E</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b87b7292-177d-417c-ae95-1b5dcf9a333c</testSuiteGuid>
   <testCaseLink>
      <guid>f2b01468-6c23-4177-a23e-3466acfab4e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01 Registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73faa807-bf9f-4d78-883d-32b4e81aa05d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02 Direct Payment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9dd4024e-4d71-461e-9bd3-0eaf99d2ea9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03 Card Payment</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
