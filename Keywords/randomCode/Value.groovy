package randomCode

import com.kms.katalon.core.annotation.Keyword

public class randomInputInteger {
	@Keyword
	def Value(int length) {
		String chars = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
		Random rand = new Random();
		StringBuilder sb = new StringBuilder();

		for (int i=0; i<length; i++) {
			sb.append(chars.charAt(rand.nextInt(chars.length())));
		}
		String result = sb.toString()
		return result;
	}
}