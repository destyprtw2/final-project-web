import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Direct Pay/Page_DemoShop/btn_buy mouse'))

WebUI.click(findTestObject('Card Pay/Page_DemoShop/btn_pay by card'))

WebUI.setText(findTestObject('Card Pay/Page_DemoShop/field_cardNum'), '2017563114667003')

WebUI.setText(findTestObject('Card Pay/Page_DemoShop/field_cardName'), 'Project Desty')

WebUI.setText(findTestObject('Card Pay/Page_DemoShop/field_cardExp'), '12/24')

WebUI.setText(findTestObject('Card Pay/Page_DemoShop/field_cvv'), '970')

WebUI.click(findTestObject('Card Pay/Page_DemoShop/btn_pay'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

