import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Direct Pay/Page_DemoShop/btn_buy mouse'))

WebUI.click(findTestObject('Direct Pay/Page_DemoShop/btn_directpay'))

WebUI.click(findTestObject('Direct Pay/Page_Payment Gateway/btn_bank2'))

WebUI.waitForElementPresent(findTestObject('Direct Pay/Page_Log in - BankSystem/field_emailbank'), 0)

WebUI.setText(findTestObject('Object Repository/Direct Pay/Page_Log in - BankSystem/field_emailbank'), 'destyprtw2@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Direct Pay/Page_Log in - BankSystem/field_passbank'), 'iFGeFYmXIrUhQZHvW7P22w==')

WebUI.click(findTestObject('Direct Pay/Page_Log in - BankSystem/btn_login'))

WebUI.click(findTestObject('Direct Pay/Page_Confirm payment - BankSystem/btn_close'))

WebUI.waitForElementClickable(findTestObject('Direct Pay/Page_Confirm payment - BankSystem/btn_pay'), 0)

WebUI.click(findTestObject('Direct Pay/Page_Confirm payment - BankSystem/btn_pay'))

WebUI.delay(5)

WebUI.navigateToUrl('https://banksystem-demoshop.herokuapp.com/')

